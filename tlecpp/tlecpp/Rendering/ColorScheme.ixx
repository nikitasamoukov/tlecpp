export module ColorScheme;


struct ColorScheme{
	string empty = "#0E1318";
	string food = "#2F7AB7";
	string wall = "gray";
	string mouth = "#DEB14D";
	string producer = "#15DE59";
	string mover = "#60D4FF";
	string killer = "#F82380";
	string armor = "#7230DB";
	string eye = "#B6C1EA";
	string eye_slit = "#0E1318";
};

export extern const ColorScheme color_scheme;
