export module Directions;

import Utils;


export struct Directions {
	static inline const int up = 0;
	static inline const int right = 1;
	static inline const int down = 2;
	static inline const int left = 3;

	static const inline vector<vector<int>> scalars = { {0,-1},{1,0},{0,1},{-1,0} };
	static int getRandomDirection() {
		return getRandomElem(4);
	}

	static auto getRandomScalar() {
		return scalars[getRandomElem(scalars.size())];
	}
	static int getOppositeDirection(int dir) {
		switch (dir) {
		case up:
			return down;
		case down:
			return up;
		case left:
			return right;
		case right:
			return left;
		}
		throw new logic_error("???");
	}
	static int rotateRight(int dir) {
		dir++;
		if (dir > 3) {
			dir = 0;
		}
		return dir;
	}
};