module Brain;

import Organism;

void Brain::organismChangeDir(shared_ptr<Organism> organism, int direction){
	organism->changeDirection(direction);
}
