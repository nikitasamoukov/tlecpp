export module Observation;

import GridCell;

export struct Observation {
	Observation(Cell* cell, int distance, int direction) :cell(cell) {
		this->distance = distance;
		this->direction = direction;
	}
	Cell* cell;
	int distance;
	int direction;
};
