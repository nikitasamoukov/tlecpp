export module Brain;

import Hyperparams;
import Directions;
import CellStates;
import Utils;
import Observation;
import Fwd;


export struct Brain {
	struct Decision {
		const static inline int neutral = 0;
		const static inline int retreat = 1;
		const static inline int chase = 2;
		static int getRandom() {
			return getRandomElem(3);
		}
		static int getRandomNonNeutral() {
			return getRandomElem(2) + 1;
		}
	};

	weak_ptr<Organism> owner;
	vector<Observation> observations;
	unordered_map<string, int> decisions;

	Brain(shared_ptr<Organism> owner) {
		this->owner = owner;

		for (auto& cell : cellStates.all) {
			decisions[cell->name] = Decision::neutral;
		}
		decisions[cellStates.food->name] = Decision::chase;
		decisions[cellStates.killer->name] = Decision::retreat;
	}

	void randomizeDecisions(bool randomize_all = false) {
		// randomize the non obvious decisions
		if (randomize_all) {
			decisions[cellStates.food->name] = Decision::getRandom();
			decisions[cellStates.killer->name] = Decision::getRandom();
		}
		decisions[cellStates.mouth->name] = Decision::getRandom();
		decisions[cellStates.producer->name] = Decision::getRandom();
		decisions[cellStates.mover->name] = Decision::getRandom();
		decisions[cellStates.armor->name] = Decision::getRandom();
		decisions[cellStates.eye->name] = Decision::getRandom();
	}

	void observe(Observation observation) {
		observations.push_back(observation);
	}

	bool decide() {
		int decision = Decision::neutral;
		int closest = hyperparams.lookRange + 1;
		int move_direction = 0;
		for (auto& obs :this->observations) {
			if (obs.cell == nullptr || obs.cell->owner.lock() == this->owner.lock()) {
				continue;
			}
			if (obs.distance < closest) {
				decision = this->decisions[obs.cell->state->name];
				move_direction = obs.direction;
				closest = obs.distance;
			}
		}
		observations.clear();
		if (decision == Decision::chase) {
			organismChangeDir(this->owner.lock(),move_direction);
			return true;
		}
		else if (decision == Decision::retreat) {
			organismChangeDir(this->owner.lock(),Directions::getOppositeDirection(move_direction));
			return true;
		}
		return false;
	}

	void organismChangeDir(shared_ptr<Organism> organism, int direction);

	void mutate() {
		this->decisions[CellStates::getRandomName()] = Decision::getRandom();
		this->decisions[cellStates.empty->name] = Decision::neutral; // if the empty cell has a decision it gets weird
	}
};