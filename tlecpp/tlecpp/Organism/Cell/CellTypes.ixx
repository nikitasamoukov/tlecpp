export module CellTypes;

export enum class CellTypes {
	Mouth,
	Producer,
	Mover,
	Killer,
	Armor,
	Eye,
};