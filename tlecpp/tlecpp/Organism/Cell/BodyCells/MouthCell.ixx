export module MouthCell;

import CellStates;
import BodyCell;
import Hyperparams;
import Fwd;

export struct MouthCell : BodyCell {
	MouthCell(shared_ptr<Organism> org, int loc_col, int loc_row)
		: BodyCell(cellStates.mouth, org, loc_col, loc_row) {
	}

	MouthCell& operator=(MouthCell const&) = default;

	virtual void initInherit(shared_ptr<BodyCell> parent) override {
		*this = *dynamic_pointer_cast<remove_cvref_t<decltype(*this)>>(parent);
	}
	virtual void performFunction() override;

	void eatNeighbor(Cell* n_cell, shared_ptr<Environment> env);
};