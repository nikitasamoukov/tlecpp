module BodyCell;

import Fwd;
import Organism;

#include <cassert>

BodyCell::BodyCell(shared_ptr<CellState> state, shared_ptr<Organism> org, int loc_col, int loc_row) {
	assert(org);
	this->state = state;
	this->org = org;
	this->loc_col = loc_col;
	this->loc_row = loc_row;

	int distance = max(abs(loc_row) * 2 + 2, abs(loc_col) * 2 + 2);
	if (this->org.lock()->anatomy->birth_distance < distance) {
		this->org.lock()->anatomy->birth_distance = distance;
	}
}

int BodyCell::getRealCol() {
	return this->org.lock()->c + this->rotatedCol(this->org.lock()->rotation);
}

int BodyCell::getRealRow() {
	return this->org.lock()->r + this->rotatedRow(this->org.lock()->rotation);
}

Cell* BodyCell::getRealCell() {
	int real_c = this->getRealCol();
	int real_r = this->getRealRow();
	return this->org.lock()->env.lock()->grid_map->cellAt(real_c, real_r);
}