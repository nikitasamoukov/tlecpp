export module ProducerCell;

import CellStates;
import BodyCell;
import Hyperparams;

export struct ProducerCell : BodyCell {
	ProducerCell(shared_ptr<Organism> org, int loc_col, int loc_row);
	
	ProducerCell& operator=(ProducerCell const&) = default;

	virtual void initInherit(shared_ptr<BodyCell> parent) override {
		*this = *dynamic_pointer_cast<remove_cvref_t<decltype(*this)>>(parent);
	}
	void performFunction();
};
