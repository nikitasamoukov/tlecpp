module KillerCell;

import Organism;

void KillerCell::performFunction() {
	auto env = this->org.lock()->env;
	auto c = this->getRealCol();
	auto r = this->getRealRow();
	for (auto& loc : hyperparams.killableNeighbors) {
		auto cell = env.lock()->grid_map->cellAt(c + loc[0], r + loc[1]);
		this->killNeighbor(cell);
	}
}

void KillerCell::killNeighbor(Cell* n_cell) {
	if (n_cell == nullptr ||
		n_cell->owner.lock() == nullptr ||
		n_cell->owner.lock() == this->org.lock() ||
		!n_cell->owner.lock()->living ||
		n_cell->state == cellStates.armor)
		return;
	auto is_hit = n_cell->state == cellStates.killer; // has to be calculated before death
	n_cell->owner.lock()->harm();
	if (hyperparams.instaKill && is_hit) {
		this->org.lock()->harm();
	}
}