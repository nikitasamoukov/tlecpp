export module EyeCell;

import CellStates;
import BodyCell;
import Hyperparams;
import Directions;
import Observation;

export struct EyeCell : BodyCell {
	EyeCell(shared_ptr<Organism> org, int loc_col, int loc_row);
	int direction = Directions::up;

	EyeCell& operator=(EyeCell const&) = default;

	virtual void initInherit(shared_ptr<BodyCell> parent) override {
		*this = *dynamic_pointer_cast<remove_cvref_t<decltype(*this)>>(parent);
	}

	virtual void initRandom() override {
		// initialize values randomly
		this->direction = Directions::getRandomDirection();
	}

	int getAbsoluteDirection();

	virtual void performFunction() override;

	Observation look();
};
