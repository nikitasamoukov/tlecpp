export module KillerCell;

import CellStates;
import BodyCell;
import Hyperparams;

export struct KillerCell : BodyCell {
	KillerCell(shared_ptr<Organism> org, int loc_col, int loc_row)
		: BodyCell(cellStates.killer, org, loc_col, loc_row) {
	}

	KillerCell& operator=(KillerCell const&) = default;

	virtual void initInherit(shared_ptr<BodyCell> parent) override {
		*this = *dynamic_pointer_cast<remove_cvref_t<decltype(*this)>>(parent);
	}
	virtual void performFunction() override;
	void killNeighbor(Cell* n_cell);
};
