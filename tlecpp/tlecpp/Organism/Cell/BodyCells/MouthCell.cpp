module MouthCell;

import Environment;
import BodyCell;
import Organism;

void MouthCell::performFunction() {
	auto env = this->org.lock()->env;
	int real_c = this->getRealCol();
	int real_r = this->getRealRow();
	for (auto loc : hyperparams.edibleNeighbors) {
		auto cell = env.lock()->grid_map->cellAt(real_c + loc[0], real_r + loc[1]);
		this->eatNeighbor(cell, env.lock());
	}
}

void MouthCell::eatNeighbor(Cell* n_cell, shared_ptr<Environment> env) {
	if (n_cell == nullptr)
		return;
	if (n_cell->state == cellStates.food) {
		env->changeCell(n_cell->col, n_cell->row, cellStates.empty, nullptr);
		this->org.lock()->food_collected++;
	}
}
