export module ArmorCell;

import CellStates;
import BodyCell;
import Fwd;

export struct ArmorCell : BodyCell {
	ArmorCell(shared_ptr<Organism> org, int loc_col, int loc_row)
		: BodyCell(cellStates.armor, org, loc_col, loc_row) {
	}
	ArmorCell& operator=(ArmorCell const&) = default;

	virtual void initInherit(shared_ptr<BodyCell> parent) override {
		*this = *dynamic_pointer_cast<remove_cvref_t<decltype(*this)>>(parent);
	}
};
