module MoverCell;

import Organism;

MoverCell::MoverCell(shared_ptr<Organism> org, int loc_col, int loc_row)
	: BodyCell(cellStates.mover, org, loc_col, loc_row) {
	this->org.lock()->anatomy->is_mover = true;
}