export module BodyCell;

import Fwd;
import CellStates;
import Directions;
import GridCell;

// A body cell defines the relative location of the cell in it's parent organism. It also defines their functional behavior.
export struct BodyCell {
	shared_ptr<CellState> state;
	weak_ptr<Organism> org;
	int loc_col; 
	int loc_row;

	BodyCell(shared_ptr<CellState> state, shared_ptr<Organism> org, int loc_col, int loc_row);
	virtual ~BodyCell()=default;

	// TODO Mmm static creator???
	virtual void initInherit(shared_ptr<BodyCell> parent)=0;
	virtual void initRandom(){};
	virtual void performFunction(){};

	int getRealCol();

	int getRealRow();

	Cell* getRealCell();

	int rotatedCol(int dir) {
		switch (dir) {
		case Directions::up:
			return this->loc_col;
		case Directions::down:
			return this->loc_col * -1;
		case Directions::left:
			return this->loc_row;
		case Directions::right:
			return this->loc_row * -1;
		}
		throw logic_error("OOPS");
	}

	int rotatedRow(int dir) {
		switch (dir) {
		case Directions::up:
			return this->loc_row;
		case Directions::down:
			return this->loc_row * -1;
		case Directions::left:
			return this->loc_col * -1;
		case Directions::right:
			return this->loc_col;
		}
		throw logic_error("OOPS");
	}
};

