module ProducerCell;

import Organism;
import Utils;

ProducerCell::ProducerCell(shared_ptr<Organism> org, int loc_col, int loc_row)
	: BodyCell(cellStates.producer, org, loc_col, loc_row) {
	this->org.lock()->anatomy->is_producer = true;
}

void ProducerCell::performFunction() {
	if (this->org.lock()->anatomy->is_mover && !hyperparams.moversCanProduce)
		return;
	auto env = this->org.lock()->env.lock();
	auto prob = hyperparams.foodProdProb;
	auto real_c = this->getRealCol();
	auto real_r = this->getRealRow();
	if (getRandomElem(100) <= prob) {
		auto loc = hyperparams.growableNeighbors[getRandomElem(hyperparams.growableNeighbors.size())];
			auto loc_c = loc[0];
		auto loc_r = loc[1];
		auto cell = env->grid_map->cellAt(real_c + loc_c, real_r + loc_r);
		if (cell != nullptr && cell->state == cellStates.empty) {
			env->changeCell(real_c + loc_c, real_r + loc_r, cellStates.food, nullptr);
			return;
		}
	}
}