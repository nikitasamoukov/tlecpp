export module BodyCellFactory;

import MouthCell;
import ProducerCell;
import MoverCell;
import KillerCell;
import ArmorCell;
import EyeCell;
import CellStates;
import Fwd;

export struct BodyCellFactory {
	struct TypeStr {
		function<shared_ptr<BodyCell>(shared_ptr<Organism> org, int loc_col, int loc_row)> funcNew;
	};

	unordered_map<string, TypeStr>type_map;
#define FUNCMACRO(Type)  {[](shared_ptr<Organism> org, int loc_col, int loc_row){return make_shared<Type>( org,loc_col, loc_row);}}
	BodyCellFactory() {
		CellStates cellStates; // C++ is hard

		type_map[cellStates.mouth->name] = FUNCMACRO(MouthCell);
		type_map[cellStates.producer->name] = FUNCMACRO(ProducerCell);
		type_map[cellStates.mover->name] = FUNCMACRO(MoverCell);
		type_map[cellStates.killer->name] = FUNCMACRO(KillerCell);
		type_map[cellStates.armor->name] = FUNCMACRO(ArmorCell);
		type_map[cellStates.eye->name] = FUNCMACRO(EyeCell);
	}

	// create copy
	shared_ptr<BodyCell> createInherited(shared_ptr<Organism> org, shared_ptr<BodyCell>to_copy)const {
		auto cell = this->type_map.at(to_copy->state->name).funcNew(org, to_copy->loc_col, to_copy->loc_row);
		cell->initInherit(to_copy);
		cell->org = org;
		return cell;
	}

	shared_ptr<BodyCell> createRandom(shared_ptr<Organism> org, shared_ptr<CellState const> state, int  loc_col, int loc_row) const{
		auto cell = this->type_map.at(state->name).funcNew(org, loc_col, loc_row);
		cell->initRandom();
		return cell;
	}

	shared_ptr<BodyCell> createDefault(shared_ptr<Organism> org, shared_ptr<CellState const> state, int loc_col, int loc_row)const {
		auto cell = this->type_map.at(state->name).funcNew(org, loc_col, loc_row);
		return cell;
	}
};

export const inline BodyCellFactory bodyCellFactory;