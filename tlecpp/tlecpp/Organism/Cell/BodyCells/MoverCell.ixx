export module MoverCell;

import BodyCell;

export struct MoverCell : BodyCell{
    MoverCell(shared_ptr<Organism> org, int loc_col, int loc_row);

	MoverCell& operator=(MoverCell const&) = default;

	virtual void initInherit(shared_ptr<BodyCell> parent) override {
		*this = *dynamic_pointer_cast<remove_cvref_t<decltype(*this)>>(parent);
	}
};