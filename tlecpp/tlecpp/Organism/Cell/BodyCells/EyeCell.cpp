module EyeCell;

import Organism;

EyeCell::EyeCell(shared_ptr<Organism> org, int loc_col, int loc_row)
	: BodyCell(cellStates.eye, org, loc_col, loc_row) {
	this->org.lock()->anatomy->has_eyes = true;
}

int EyeCell::getAbsoluteDirection() {
	int dir = this->org.lock()->rotation + this->direction;
	if (dir > 3)
		dir -= 4;
	return dir;
}
void EyeCell::performFunction() {
	auto obs = this->look();
	this->org.lock()->brain->observe(obs);
}

Observation EyeCell::look() {
	auto env = this->org.lock()->env;
	int direction = this->getAbsoluteDirection();
	int addCol = 0;
	int addRow = 0;
	switch (direction) {
	case Directions::up:
		addRow = -1;
		break;
	case Directions::down:
		addRow = 1;
		break;
	case Directions::right:
		addCol = 1;
		break;
	case Directions::left:
		addCol = -1;
		break;
	}
	int start_col = this->getRealCol();
	int start_row = this->getRealRow();
	int col = start_col;
	int row = start_row;
	Cell* cell = nullptr;
	for (int i = 0; i < hyperparams.lookRange; i++) {
		col += addCol;
		row += addRow;
		cell = env.lock()->grid_map->cellAt(col, row);
		if (cell == nullptr) {
			break;
		}
		if (cell->state != cellStates.empty) {
			int distance = abs(start_col - col) + abs(start_row - row);
			return Observation(cell, distance, direction);
		}
	}
	return Observation(cell, hyperparams.lookRange, direction);
}
