export module CellStates;

import Utils;

export struct CellState {
	string name;

	CellState(string name) {
		this->name = name;
	}

	virtual ~CellState() = default;
};

export struct Empty : CellState {
	Empty() :CellState("empty") {
	}
};
export struct Food : CellState {
	Food() :CellState("food") {
	}
};
export struct Wall : CellState {
	Wall() :CellState("wall") {
	}
};
export struct Mouth : CellState {
	Mouth() :CellState("mouth") {
	}
};
export struct Producer :CellState {
	Producer() :CellState("producer") {
	}
};
export struct Mover : CellState {
	Mover() :CellState("mover") {
	}
};
export struct Killer :CellState {
	Killer() :CellState("killer") {
	}
};
export struct Armor :CellState {
	Armor() :CellState("armor") {
	}
};
export struct Eye : CellState {
	Eye() :CellState("eye") {
	}
};

export struct CellStates {
	shared_ptr<Empty> empty = make_shared<Empty>();
	shared_ptr<Food> food = make_shared<Food>();
	shared_ptr<Wall> wall = make_shared<Wall>();
	shared_ptr<Mouth> mouth = make_shared<Mouth>();
	shared_ptr<Producer> producer = make_shared<Producer>();
	shared_ptr<Mover> mover = make_shared<Mover>();
	shared_ptr<Killer> killer = make_shared<Killer>();
	shared_ptr<Armor> armor = make_shared<Armor>();
	shared_ptr<Eye> eye = make_shared<Eye>();
	
	vector<shared_ptr<CellState>> all = { empty, food, wall, mouth, producer, mover, killer, armor, eye };
	vector<shared_ptr<CellState>> living = { mouth, producer, mover, killer, armor, eye };

	static shared_ptr<CellState> getRandomLivingType();
	static string getRandomName();
};

export const inline CellStates cellStates;

string CellStates::getRandomName() {
	return cellStates.all[getRandomElem(cellStates.all.size())]->name;
}

shared_ptr<CellState> CellStates::getRandomLivingType() {
	return cellStates.living[getRandomElem(cellStates.living.size())];
}
