export module GridCell;

import CellStates;
import Hyperparams;
import Fwd;

export struct Cell {
	weak_ptr<Organism> owner; // owner organism
	weak_ptr<BodyCell> cell_owner;
	shared_ptr<CellState const> state;
	int col = 0;
	int row = 0;

	Cell(shared_ptr<CellState const> state, int col, int row) {
		setType(state);
		col = col;
		row = row;
	}

	void setType(shared_ptr< CellState const> state) {
		this->state = state;
	}
};
