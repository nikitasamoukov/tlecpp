export module Organism;

export import Directions;
import CellStates;
import Neighbors;
import Hyperparams;
import Anatomy;
import Brain;
import Environment;
import Utils;


#include <cassert>

export struct Organism :std::enable_shared_from_this<Organism> {
private:
	Organism() = default;
public:
	int c = 0;
	int r = 0;
	weak_ptr<Environment> env;
	int lifetime = 0;
	int food_collected = 0;
	bool living = true;
	shared_ptr<Anatomy> anatomy;
	int direction = Directions::down; // direction of movement
	int rotation = Directions::up; // direction of rotation
	bool can_rotate = false;
	int move_count = 0;
	int move_range = 4;
	int ignore_brain_for = 0;
	int mutability = 5;
	int damage = 0;
	shared_ptr<Brain> brain;

	static shared_ptr<Organism> create(int col, int row, shared_ptr<Environment> env, shared_ptr<Organism> parent = nullptr) {
        struct make_shared_enabler : public Organism {};
		auto org = make_shared<make_shared_enabler>();
		org->c = col;
		org->r = row;
		org->env = env;
		org->living = true;
		org->anatomy = make_shared<Anatomy>(org);
		org->can_rotate = hyperparams.rotationEnabled;
		org->brain = make_shared<Brain>(org);
		if (parent) {
			org->inherit(parent);
		}

		return org;
	}

	void inherit(shared_ptr<Organism> parent) {
		this->move_range = parent->move_range;
		this->mutability = parent->mutability;
		for (auto& c : parent->anatomy->cells) {
			//deep copy parent cells
			this->anatomy->addInheritCell(c);
		}
		if (parent->anatomy->is_mover && parent->anatomy->has_eyes) {
			*this->brain = *parent->brain;
		}
	}

	// amount of food required before it can reproduce
	int foodNeeded() {
		return this->anatomy->is_mover ? this->anatomy->cells.size() + hyperparams.extraMoverFoodCost : this->anatomy->cells.size();
	}

	int lifespan() {
		return this->anatomy->cells.size() * hyperparams.lifespanMultiplier;
	}

	int maxHealth() {
		return this->anatomy->cells.size();
	}

	void reproduce() {
		//produce mutated child
		//check nearby locations (is there room and a direct path)
		auto org = create(0, 0, this->env.lock(), shared_from_this());
		
		for (auto& cell : org->anatomy->cells) {
			assert(cell->org.lock() == org);
		}
		check();

		if (hyperparams.rotationEnabled) {
			org->rotation = Directions::getRandomDirection();
		}
		auto prob = this->mutability;
		if (hyperparams.useGlobalMutability) {
			prob = hyperparams.globalMutability;
		}
		else {
			//mutate the mutability
			if (getRandomElem(2) == 1)
				org->mutability++;
			else {
				org->mutability--;
				if (org->mutability < 1)
					org->mutability = 1;
			}
		}
		check();
		bool mutated = false;
		if (calcRandomChance(prob)) {
			if (org->anatomy->is_mover && getRandomElem(100) <= 10) {
				if (org->anatomy->has_eyes) {
					org->brain->mutate();
				}
				org->move_range += getRandomElem(5) - 2;
				if (org->move_range <= 0) {
					org->move_range = 1;
				}
				if (org->move_range >= 50)
					org->move_range = 50;

			}
			else {
				mutated = org->mutate();
			}
		}

		auto direction = Directions::getRandomScalar();
		int direction_c = direction[0];
		int direction_r = direction[1];
		int offset = getRandomElem(3);
		int basemovement = this->anatomy->birth_distance;
		int new_c = this->c + (direction_c * basemovement) + (direction_c * offset);
		int new_r = this->r + (direction_r * basemovement) + (direction_r * offset);
		
		check();
		if (org->isClear(new_c, new_r, org->rotation) && org->isStraightPath(new_c, new_r, this->c, this->r, shared_from_this())) {
			org->c = new_c;
			org->r = new_r;
			

			
			check();
			for (auto& cell : org->anatomy->cells) {
				assert(cell->org.lock() == org);
			}
			this->env.lock()->addOrganism(org);
			check();
			org->updateGrid();
		}
		check();
		this->food_collected = max(this->food_collected - this->foodNeeded(), 0);
	}

	void check();

	bool mutate() {
		bool added = false;
		bool changed = false;
		bool removed = false;
		if (this->calcRandomChance(hyperparams.addProb)) {
			auto branch = this->anatomy->getRandomCell();
			auto state = cellStates.getRandomLivingType();//branch->state;
			auto growth_direction = neighbors.all[getRandomElem(neighbors.all.size())];
			auto c = branch->loc_col + growth_direction[0];
			auto r = branch->loc_row + growth_direction[1];
			if (this->anatomy->canAddCellAt(c, r)) {
				added = true;
				this->anatomy->addRandomizedCell(state, c, r);
			}
		}
		if (this->calcRandomChance(hyperparams.changeProb)) {
			auto cell = this->anatomy->getRandomCell();
			auto state = cellStates.getRandomLivingType();
			this->anatomy->replaceCell(state, cell->loc_col, cell->loc_row);
			changed = true;
		}
		if (this->calcRandomChance(hyperparams.removeProb)) {
			if (this->anatomy->cells.size() > 1) {
				auto cell = this->anatomy->getRandomCell();
				removed = this->anatomy->removeCell(cell->loc_col, cell->loc_row);
			}
		}
		return added || changed || removed;
	}

	static bool calcRandomChance(int prob) {
		return getRandomElem(100) < prob;
	}

	bool attemptMove() {
		auto direction = Directions::scalars[this->direction];
		auto direction_c = direction[0];
		auto direction_r = direction[1];
		auto new_c = this->c + direction_c;
		auto new_r = this->r + direction_r;
		if (this->isClear(new_c, new_r, this->rotation)) {
			for (auto& cell : this->anatomy->cells) {
				auto real_c = this->c + cell->rotatedCol(this->rotation);
				auto real_r = this->r + cell->rotatedRow(this->rotation);
				this->env.lock()->changeCell(real_c, real_r, cellStates.empty, nullptr);
			}
			this->c = new_c;
			this->r = new_r;
			this->updateGrid();
			return true;
		}
		return false;
	}

	bool attemptRotate() {
		if (!this->can_rotate) {
			this->direction = Directions::getRandomDirection();
			this->move_count = 0;
			return true;
		}
		auto new_rotation = Directions::getRandomDirection();
		if (this->isClear(this->c, this->r, new_rotation)) {
			for (auto cell : this->anatomy->cells) {
				auto real_c = this->c + cell->rotatedCol(this->rotation);
				auto real_r = this->r + cell->rotatedRow(this->rotation);
				this->env.lock()->changeCell(real_c, real_r, cellStates.empty, nullptr);
			}
			this->rotation = new_rotation;
			this->direction = Directions::getRandomDirection();
			this->updateGrid();
			this->move_count = 0;
			return true;
		}
		return false;
	}

	void changeDirection(int dir) {
		this->direction = dir;
		this->move_count = 0;
	}

	// assumes either c1==c2 or r1==r2, returns true if there is a clear path from point 1 to 2
	bool isStraightPath(int c1, int r1, int c2, int r2, shared_ptr<Organism> parent) {
		if (c1 == c2) {
			if (r1 > r2) {
				auto temp = r2;
				r2 = r1;
				r1 = temp;
			}
			for (auto i = r1; i != r2; i++) {
				auto cell = this->env.lock()->grid_map->cellAt(c1, i);
				if (!this->isPassableCell(cell, parent)) {
					return false;
				}
			}
			return true;
		}
		else if (r1 == r2) {
			if (c1 > c2) {
				auto temp = c2;
				c2 = c1;
				c1 = temp;
			}
			for (auto i = c1; i != c2; i++) {
				auto cell = this->env.lock()->grid_map->cellAt(i, r1);
				if (!this->isPassableCell(cell, parent)) {
					return false;
				}
			}
			return true;
		}
		else {
			throw logic_error("oops");
		}
	}

	static bool isPassableCell(Cell* cell, shared_ptr<Organism> parent) {
		return cell != nullptr && (cell->state == cellStates.empty || cell->owner.lock() == parent || cell->state == cellStates.food);
	}

	bool isClear(int col, int row, int rotation /*= this->rotation*/) {
		for (auto& loccell : this->anatomy->cells) {
			auto cell = this->getRealCell(loccell, col, row, rotation);
			if (cell == nullptr) {
				return false;
			}
			if (cell->owner.lock().get() == this || cell->state == cellStates.empty || (!hyperparams.foodBlocksReproduction && cell->state == cellStates.food)) {
				continue;
			}
			return false;
		}
		return true;
	}

	void harm() {
		this->damage++;
		if (this->damage >= this->maxHealth() || hyperparams.instaKill) {
			this->die();
		}
	}

	void die() {
		for (auto& cell : this->anatomy->cells) {
			auto real_c = this->c + cell->rotatedCol(this->rotation);
			auto real_r = this->r + cell->rotatedRow(this->rotation);
			this->env.lock()->changeCell(real_c, real_r, cellStates.food, nullptr);
		}
		this->living = false;
	}

	void updateGrid() {
		for (auto& cell : this->anatomy->cells) {
			auto real_c = this->c + cell->rotatedCol(this->rotation);
			auto real_r = this->r + cell->rotatedRow(this->rotation);
			this->env.lock()->changeCell(real_c, real_r, cell->state, cell);
		}
	}

	bool update() {
		check();
		this->lifetime++;
		if (this->lifetime > this->lifespan()) {
			this->die();
			return this->living;
		}
		check();
		if (this->food_collected >= this->foodNeeded()) {
			this->reproduce();
		}
		check();
		for (auto& cell : this->anatomy->cells) {
			cell->performFunction();
			check();
			if (!this->living)
				return this->living;
		}
		check();

		if (this->anatomy->is_mover) {
			this->move_count++;
			auto changed_dir = false;
			if (this->ignore_brain_for == 0) {
				changed_dir = this->brain->decide();
			}
			else {
				this->ignore_brain_for--;
			}
			auto moved = this->attemptMove();
			if ((this->move_count > this->move_range && !changed_dir) || !moved) {
				auto rotated = this->attemptRotate();
				if (!rotated) {
					this->changeDirection(Directions::getRandomDirection());
					if (changed_dir)
						this->ignore_brain_for = this->move_range + 1;
				}
			}
		}
		check();

		return this->living;
	}

	/*
	c = this->c, r = this->r, rotation = this->rotation
	*/
	Cell* getRealCell(shared_ptr<BodyCell> local_cell, int c, int r, int rotation) {
		auto real_c = c + local_cell->rotatedCol(rotation);
		auto real_r = r + local_cell->rotatedRow(rotation);
		return this->env.lock()->grid_map->cellAt(real_c, real_r);
	}
};
