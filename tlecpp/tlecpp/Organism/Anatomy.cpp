module Anatomy;

import BodyCell;
import BodyCellFactory;
import Organism;

shared_ptr<BodyCell> Anatomy::addRandomizedCell(shared_ptr<CellState>state, int c, int r) {
	if (state == cellStates.eye && !this->has_eyes) {
		this->owner.lock()->brain->randomizeDecisions();
	}
	auto new_cell = bodyCellFactory.createRandom(this->owner.lock(), state, c, r);
	this->cells.push_back(new_cell);
	return new_cell;
}
