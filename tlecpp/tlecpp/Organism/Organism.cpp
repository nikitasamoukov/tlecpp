module Organism;

import WorldEnvironment;

#include<cassert>

void Organism::check() {
	for (auto& cell : anatomy->cells) {
		assert(cell->org.lock().get() == this);
	}

	dynamic_cast<WorldEnvironment*>(env.lock().get())->check();

}
