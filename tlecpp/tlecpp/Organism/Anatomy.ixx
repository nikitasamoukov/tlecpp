export module Anatomy;

import Fwd;
import CellStates;
import BodyCell;
import BodyCellFactory;
import Utils;

export struct Organism;

export struct Anatomy {
private:
	Anatomy() = default;
public:
	Anatomy(shared_ptr<Organism> owner) {
		this->owner = owner;
		this->birth_distance = 4;
		this->clear();
	}

	weak_ptr<Organism> owner;
	int birth_distance = 4;

	vector<shared_ptr<BodyCell>> cells;
	bool is_producer = false;
	bool is_mover = false;
	bool has_eyes = false;

	void clear() {
		cells.clear();
		is_producer = false;
		is_mover = false;
		has_eyes = false;
	}

	bool canAddCellAt(int c, int r) {
		for (auto& cell : this->cells) {
			if (cell->loc_col == c && cell->loc_row == r) {
				return false;
			}
		}
		return true;
	}

	shared_ptr<BodyCell> addDefaultCell(shared_ptr<CellState> state, int c, int r) {
		shared_ptr<BodyCell> new_cell = bodyCellFactory.createDefault(this->owner.lock(), state, c, r);
		cells.push_back(new_cell);
		return new_cell;
	}

	shared_ptr<BodyCell> addRandomizedCell(shared_ptr<CellState>state, int c, int r);

	shared_ptr<BodyCell> addInheritCell(shared_ptr<BodyCell> parent_cell) {
		auto new_cell = bodyCellFactory.createInherited(this->owner.lock(), parent_cell);
		this->cells.push_back(new_cell);
		return new_cell;
	}

	shared_ptr<BodyCell>replaceCell(shared_ptr<CellState>state, int c, int r, bool randomize = true) {
		this->removeCell(c, r, true);
		if (randomize) {
			return this->addRandomizedCell(state, c, r);
		}
		else {
			return this->addDefaultCell(state, c, r);
		}
	}

	bool removeCell(int c, int r, bool allow_center_removal = false) {
		if (c == 0 && r == 0 && !allow_center_removal)
			return false;
		for (int i = 0; i < this->cells.size(); i++) {
			auto& cell = this->cells[i];
			if (cell->loc_col == c && cell->loc_row == r) {
				this->cells.erase(this->cells.begin() + i);
				i--;
				break;
			}
		}
		this->checkTypeChange();
		return true;
	}

	shared_ptr<BodyCell> getLocalCell(int c, int r) {
		for (auto& cell : this->cells) {
			if (cell->loc_col == c && cell->loc_row == r) {
				return cell;
			}
		}
		return nullptr;
	}

	void checkTypeChange() {
		this->is_producer = false;
		this->is_mover = false;
		this->has_eyes = false;
		for (auto& cell : this->cells) {
			if (cell->state == cellStates.producer)
				this->is_producer = true;
			if (cell->state == cellStates.mover)
				this->is_mover = true;
			if (cell->state == cellStates.eye)
				this->has_eyes = true;
		}
	}

	shared_ptr<BodyCell> getRandomCell() {
		return this->cells[getRandomElem(this->cells.size())];
	}

	vector<shared_ptr<BodyCell>> getNeighborsOfCell(int col, int row) {
		vector<shared_ptr<BodyCell>> neighbors;
		for (int x = -1; x <= 1; x++) {
			for (int y = -1; y <= 1; y++) {

				auto neighbor = this->getLocalCell(col + x, row + y);
				if (neighbor)
					neighbors.push_back(neighbor);
			}
		}

		return neighbors;
	}

	bool isEqual(Anatomy const& anatomy) { // currently unused helper func. inefficient, avoid usage in prod.
		if (this->cells.size() != anatomy.cells.size()) return false;
		for (int i = 0; i < this->cells.size(); i++) {
			auto my_cell = this->cells[i];
			auto their_cell = anatomy.cells[i];
			if (my_cell->loc_col != their_cell->loc_col ||
				my_cell->loc_row != their_cell->loc_row ||
				my_cell->state != their_cell->state)
				return false;
		}
		return true;
	}
};