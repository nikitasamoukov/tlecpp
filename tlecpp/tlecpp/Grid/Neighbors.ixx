export module Neighbors;

// contains local cell values for the following:

//all       ...
//          .x.
//          ...

//adjacent   .
//          .x.
//           .

//corners   . .
//           x
//          . .

//allSelf   ...
//          ...
//          ...

export struct Neighbors {
	 const vector<vector<int>> all = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}, {-1, -1}, {1, 1}, {-1, 1}, {1, -1}};
	 const vector<vector<int>> adjacent = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
	 const vector<vector<int>> corners = {{-1, -1}, {1, 1}, {-1, 1}, {1, -1}};
	 const vector<vector<int>> allSelf = {{0, 0}, {0, 1}, {0, -1}, {1, 0}, {-1, 0}, {-1, -1}, {1, 1}, {-1, 1}, {1, -1}};
};

export inline Neighbors neighbors;

