export module GridMap;

import GridCell;
import CellStates;
import Fwd;

export struct GridMap {
	vector<vector<Cell>> grid;
	int cols = 0;
	int rows = 0;

	GridMap(int cols, int rows) {
		resize(cols, rows);
	}

	void resize(int cols, int rows) {
		grid.resize(0);
		this->cols = cols;
		this->rows = rows;
		for (int c = 0; c < cols; c++) {
			vector<Cell> row;
			for (int r = 0; r < rows; r++) {
				Cell cell(cellStates.empty, c, r);
				row.push_back(cell);
			}
			grid.push_back(row);
		}
	}

	void fillGrid(shared_ptr<CellState const> state, bool ignore_walls = false) {
		for (auto& col : grid) {
			for (auto& cell : col) {
				if (ignore_walls && cell.state == cellStates.wall) continue;
				cell.setType(state);
				cell.owner = {};
				cell.cell_owner = {};
			}
		}
	}

	Cell* cellAt(int col, int  row) {
		if (!isValidLoc(col, row)) {
			cout << __FILE__ << " " << __LINE__ << endl;
			return nullptr;
		}
		return &grid[col][row];
	}

	void setCellType(int col, int row, shared_ptr<CellState const> state) {
		if (!isValidLoc(col, row)) {
			cout << __FILE__ << " " << __LINE__ << endl;
			return;
		}
		grid[col][row].setType(state);
	}

	void setCellOwner(int col, int row, shared_ptr<BodyCell> cell_owner);

	bool isValidLoc(int col, int  row) {
		return col < cols&& row < rows&& col >= 0 && row >= 0;
	}

	vector<int> getCenter() {
		return{ cols / 2,rows / 2 };
	}
};
