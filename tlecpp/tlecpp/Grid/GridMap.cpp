module GridMap;

import BodyCell;

void GridMap::setCellOwner(int col, int row, shared_ptr<BodyCell> cell_owner) {
	if (!isValidLoc(col, row)) {
		cout << __FILE__ << " " << __LINE__ << endl;
		return;
	}
	auto t = weak_ptr<BodyCell>(cell_owner);
	grid[col][row].cell_owner = cell_owner;
	if (cell_owner)
		grid[col][row].owner = cell_owner->org;
	else
		grid[col][row].owner = {};
}
