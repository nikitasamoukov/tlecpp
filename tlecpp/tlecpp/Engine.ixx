export module Engine;

import WorldEnvironment;

export struct Engine {
private:
	Engine()=default;
public:
	shared_ptr<WorldEnvironment> env;
	bool running = false;
	
	static shared_ptr<Engine> create(){
		struct make_shared_enabler : public Engine {};
		auto inst = make_shared<make_shared_enabler>();
		inst->env= make_shared<WorldEnvironment>(inst);
		inst->env->OriginOfLife();
		return inst;
	}

	void loop() {
		environmentUpdate();
	}

	void start(float fps = 60) {
		if (fps <= 0)
			fps = 1;
		fps = fps;
		running = true;

	}

	void stop() {
		running = false;
	}

	void restart(float fps) {
		start(fps);
	}

	void environmentUpdate() {
		env->update();
		necessaryUpdate();
	}

	void necessaryUpdate() {
	}

};
