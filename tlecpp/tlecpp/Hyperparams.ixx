export module Hyperparams;

import Neighbors;

export struct Hyperparams {

	float lifespanMultiplier = 100;
	float foodProdProb = 5;

	Neighbors neighbors; // C++ is hard

	vector<vector<int>> killableNeighbors = neighbors.adjacent;
	vector<vector<int>> edibleNeighbors = neighbors.adjacent;
	vector<vector<int>> growableNeighbors = neighbors.adjacent;

	bool useGlobalMutability = false;
	int globalMutability = 5;
	float addProb = 33;
	float changeProb = 33;
	float removeProb = 33;

	bool rotationEnabled = true;

	bool foodBlocksReproduction = true;
	bool moversCanProduce = false;

	bool instaKill = false;

	int lookRange = 20;

	float foodDropProb = 0;

	float extraMoverFoodCost = 0;
};

export extern Hyperparams hyperparams;
