export module WorldEnvironment;

import Environment;
import GridMap;
import Organism;
import CellStates;
import Hyperparams;
import WorldConfig;
import Fwd;
import Utils;

#include <cassert>

export struct WorldEnvironment : Environment, enable_shared_from_this<WorldEnvironment> {
	weak_ptr<Engine> engine;

	int num_rows = 0;
	int num_cols = 0;
	vector<shared_ptr<Organism>> organisms;
	vector<Cell*>walls;
	int total_ticks = 0;

	WorldEnvironment(shared_ptr<Engine> engine) :engine(engine) {
		this->num_rows = 300;
		this->num_cols = 300;
		this->grid_map = make_shared<GridMap>(num_cols, num_rows);
		this->total_ticks = 0;
	}

	void update() {
		check();
		vector<int> to_remove;
		for (int i = 0; i < this->organisms.size(); i++) {
			check();
			auto org = this->organisms[i];
			if (!org->living || !org->update()) {
				to_remove.push_back(i);
			}
		}
		check();
		this->removeOrganisms(to_remove);
		if (hyperparams.foodDropProb > 0) {
			this->generateFood();
		}
		this->total_ticks++;

		check();
	}

	void check(){
		for(auto& org:organisms){
			for(auto& cell:org->anatomy->cells){
				assert(cell->org.lock()==org);
			}
		}
	}

	void removeOrganisms(vector<int> org_indeces) {
		auto start_pop = this->organisms.size();
		for (int i = org_indeces.size() - 1; i >= 0; i--) {
			this->organisms.erase(this->organisms.begin() + i);
		}
		if (this->organisms.size() == 0 && start_pop > 0) {
			if (worldConfig.auto_pause)
				pauseEngine();
			else if (worldConfig.auto_reset) {
				this->reset(false);
			}
		}
	}

	void pauseEngine();

	void OriginOfLife() {
		auto center = this->grid_map->getCenter();
		auto org = Organism::create(center[0], center[1], shared_from_this());
		org->anatomy->addDefaultCell(cellStates.mouth, 0, 0);
		org->anatomy->addDefaultCell(cellStates.producer, 1, 1);
		org->anatomy->addDefaultCell(cellStates.producer, -1, -1);
		this->addOrganism(org);
	}

	void addOrganism(shared_ptr<Organism> organism) {
		organism->updateGrid();
		this->organisms.push_back(organism);
	}

	void changeCell(int c, int  r, shared_ptr<CellState> state, shared_ptr<BodyCell> owner_cell) {
		Environment::changeCell(c, r, state, owner_cell);
		if (state == cellStates.wall)
			this->walls.push_back(this->grid_map->cellAt(c, r));
	}

	void clearWalls() {
		for (auto& wall : this->walls) {
			auto wcell = this->grid_map->cellAt(wall->col, wall->row);
			if (wcell && wcell->state == cellStates.wall)
				this->changeCell(wall->col, wall->row, cellStates.empty, nullptr);
		}
	}

	void clearOrganisms() {
		for (auto& org : this->organisms)
			org->die();
		this->organisms = {};
	}

	void clearDeadOrganisms() {
		vector<int> to_remove;
		for (int i = 0; i < this->organisms.size(); i++) {
			auto org = this->organisms[i];
			if (!org->living)
				to_remove.push_back(i);
		}
		this->removeOrganisms(to_remove);
	}

	void generateFood() {
		auto num_food = max<float>(floor(this->grid_map->cols * this->grid_map->rows * hyperparams.foodDropProb / 50000.0f), 1);
		auto prob = hyperparams.foodDropProb;
		for (auto i = 0; i < num_food; i++) {
			if (getRandomElem(100) <= prob) {
				int c = floor(getRandomElem( this->grid_map->cols));
				int r = floor(getRandomElem( this->grid_map->rows));

				if (this->grid_map->cellAt(c, r)->state == cellStates.empty) {
					this->changeCell(c, r, cellStates.food, nullptr);
				}
			}
		}
	}

	bool reset(bool confirm_reset = true, bool reset_life = true) {

		organisms.clear();
		this->grid_map->fillGrid(cellStates.empty, !worldConfig.clear_walls_on_reset);
		this->total_ticks = 0;
		if (reset_life)
			this->OriginOfLife();
		return true;
	}

	void resizeGridColRow(int cols, int rows) {
		this->grid_map->resize(cols, rows);
	}
};
