module WorldEnvironment;

import Engine;

void WorldEnvironment::pauseEngine() {
	engine.lock()->running ^= true;
}