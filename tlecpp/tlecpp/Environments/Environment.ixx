export module Environment;

import GridMap;
import CellStates;
import Fwd;

export struct Environment {
	shared_ptr<GridMap> grid_map;

	void changeCell(int c, int r, shared_ptr<CellState const> state, shared_ptr<BodyCell> cell_owner) {
		grid_map->setCellType(c, r, state);
		grid_map->setCellOwner(c, r, cell_owner);
	}

	virtual void addOrganism(shared_ptr<Organism> organism) = 0;
};
