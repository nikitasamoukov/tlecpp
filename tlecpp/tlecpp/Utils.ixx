export module Utils;

export int getRandomElem(int array_size) {
	if (array_size <= 0)
		throw new logic_error("zero size array");
	static std::mt19937_64 generator(std::random_device{}());
	std::uniform_int_distribution<std::size_t> distribution(0, array_size - 1);
	return distribution(generator);
}