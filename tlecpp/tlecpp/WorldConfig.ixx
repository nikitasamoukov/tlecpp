export module WorldConfig;

export struct WorldConfig {
	bool headless = false;
	bool clear_walls_on_reset = false;
	bool auto_reset = true;
	bool auto_pause = false;
};
export extern WorldConfig worldConfig;