
import Engine;
import Neighbors;

int main()
{

	cout<<neighbors.adjacent.size()<<endl;

	vector<vector<int>> adjacent = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};

	auto engine = Engine::create();
	engine->start();
	while (1)
		engine->loop();
	return 0;
}
